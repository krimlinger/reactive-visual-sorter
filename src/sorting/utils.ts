export type SortIteratorValue = {
  array: number[]
  swap?: [number, number]
}

export function* swap(arr: number[], i: number, j: number) {
  [arr[i], arr[j]] = [arr[j], arr[i]]
  yield { array: [...arr], swap: [i, j] as [number, number] }
}

// Consume a sort generator and get its last value (sorted array)
export function consumeSortGenerator(gen: Generator<SortIteratorValue, void>) {
  let ret: number[] = []
  for (const arr of gen) {
    ret = [...arr.array]
  }
  return ret
}

export function generateRandomizedArray(size: number): number[] {
  if (size < 0) {
    return []
  }
  const numbers = [...Array(size).keys()]
  const arr = []
  for (let i = size; i > 0; i--) {
    arr.push(numbers.splice(Math.floor(Math.random() * i), 1)[0])
  }
  return arr
}

export function isSorted(arr: number[]) {
  if (arr.length < 2) {
    return true
  }
  for (let i=1; i < arr.length; i++) {
    if (arr[i-1] > arr[i]) {
      return false
    }
  }
  return true
}

export function generateSortTests(
  sortF: (arr: number[]) => Generator<SortIteratorValue, void>,
  name = sortF.name) {
  test(`sort empty array using ${name}`, () => {
    const arr = generateRandomizedArray(0);
    expect(consumeSortGenerator(sortF(arr))).toEqual([])
  })

  test(`sort singleton array using ${name}`, () => {
    const arr = generateRandomizedArray(1)
    expect(consumeSortGenerator(sortF(arr))).toEqual([0])
  })

  test(`sort array of 10 elements using ${name}`, () => {
    const arr = generateRandomizedArray(10)
    expect(consumeSortGenerator(sortF(arr))).toEqual([...Array(10).keys()])
  })
}
