import {swap} from './utils'

export function* insertionSort(array: number[]) {
  const arr = [...array]
  if (arr.length < 1) {
    yield { array: arr }
    return
  }
  for (let i = 1; i < arr.length; i++) {
    let j = i
    while (j > 0 && arr[j - 1] > arr[j]) {
      yield* swap(arr, j, j - 1)
      j -= 1
    }
  }
  yield { array: arr }
}
