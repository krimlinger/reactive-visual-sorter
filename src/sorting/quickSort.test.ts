import {partition, quickSort} from './quickSort'
import {
  generateSortTests,
  generateRandomizedArray
} from './utils'

test('partition empty array', () => {
  const arr: number[] = []
  const pp = {p: -1}
  for (const _ of partition(arr, 0, 0, pp)) {}
  expect(arr).toEqual([])
})

test('partition simple array', () => {
  const arr = [1, 0]
  const pp = {p: -1}
  for (const _ of partition(arr, 0, 1, pp)) {}
  expect(arr).toEqual([0, 1])
})

test('partition more complex array', () => {
  const arr = generateRandomizedArray(10)
  const mid = {p: -1}
  for (const _ of partition(arr, 0, arr.length - 1, mid)) {}
  expect(arr.slice(0, mid.p).every((x) => x <= arr[mid.p])).toBeTruthy()
  expect(arr.slice(mid.p).every((x) => x >= arr[mid.p])).toBeTruthy()
})

generateSortTests(quickSort)
