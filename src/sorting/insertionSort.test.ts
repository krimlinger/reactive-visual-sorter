import {insertionSort} from './insertionSort'
import {generateSortTests} from './utils'

generateSortTests(insertionSort)
