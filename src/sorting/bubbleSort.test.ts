import {bubbleSort} from './bubbleSort'
import {generateSortTests} from './utils'

generateSortTests(bubbleSort)
