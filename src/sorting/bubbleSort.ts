import {swap} from './utils'

export function* bubbleSort(array: number[]) {
  const arr = [...array]
  if (arr.length < 2) {
    yield { array: arr }
    return
  }
  let didSwap = true;
  while (didSwap) {
    didSwap = false;
    for (let i = 1; i < arr.length; i++) {
      if (arr[i - 1] > arr[i]) {
        didSwap = true;
        yield* swap(arr, i, i - 1)
      }
    }
  }
  yield { array: arr }
}
