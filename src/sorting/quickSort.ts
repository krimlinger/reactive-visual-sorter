import {SortIteratorValue, swap} from './utils'

export function* quickSort(array: number[]) {
  const arr = [...array]
  yield* qsort(arr, 0, arr.length - 1)
  yield {array: [...arr]}
}

function* qsort(arr: number[], l: number, h: number): Generator<SortIteratorValue, void> {
  if (l >= h) {
    return
  }
  let p = {p: -1}
  yield* partition(arr, l, h, p)
  // No need for p-1 as the partition function uses Math.floor
  yield* qsort(arr, l, p.p)
  yield* qsort(arr, p.p + 1, h)
}

export function* partition(arr: number[], l: number, h: number, pp: {p: number}) {
  if (l >= h) {
    pp.p = l
    return
  }
  // Take the middle value just in case the array is already sorted.
  let p = Math.floor((h + l) / 2),
    t = l,
    pval = arr[p];
  yield* swap(arr, p, h)
  for (let i = l; i < h; i++) {
    if (arr[i] < pval) {
      yield* swap(arr, i, t)
      t++
    }
  }
  yield* swap(arr, t, h)
  pp.p = t
}
