import {bubbleSort} from './bubbleSort'
import {insertionSort} from './insertionSort'
import {quickSort} from './quickSort'
import {selectionSort} from './selectionSort'

export enum SortType {
  Bubble = 'buble',
  Insertion = 'insertion',
  Quick = 'quick',
  Selection = 'selection',
}

export function sortTypeToFunc(sortType: SortType) {
  switch (sortType) {
  case SortType.Bubble:
    return bubbleSort
  case SortType.Insertion:
    return insertionSort
  case SortType.Quick:
    return quickSort
  case SortType.Selection:
    return selectionSort
  default:
    throw new Error('unknown SortType in sortTypeToFunc:' + sortType)
  }
}

export function stringToSortType(sortType: string) {
  switch (sortType) {
    case 'bubble':
      return SortType.Bubble
    case 'insertion':
      return SortType.Insertion
    case 'quick':
      return SortType.Quick
    case 'selection':
      return SortType.Selection
    default:
      return null
  }
}

export type SortOptions = {
  sampleSize: number
  sortType: SortType
  isRunning: boolean
}

export type PartialSortOptions = Partial<SortOptions>

export const defaultSortOptions: SortOptions = {
  sampleSize: 10,
  sortType: SortType.Quick,
  isRunning: false,
}
