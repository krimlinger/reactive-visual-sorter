import {selectionSort} from './selectionSort'
import {generateSortTests} from './utils'

generateSortTests(selectionSort)
