import {
  consumeSortGenerator,
  generateRandomizedArray,
  swap
} from './utils'

let swapArr: number[] = []

beforeEach(() => {
  swapArr = [1, 2]
})

test('swap two indices', () => {
  consumeSortGenerator(swap(swapArr, 0, 1))
  expect(swapArr).toEqual([2, 1])
})

test('swap same indices', () => {
  consumeSortGenerator(swap(swapArr, 0, 0))
  expect(swapArr).toEqual([1, 2])
})

test('generate negative index randomized array', () => {
  expect(generateRandomizedArray(-1)).toEqual([])
})

test('generate empty randomized array', () => {
  expect(generateRandomizedArray(0)).toEqual([])
})

test('generate singleton randomized array', () => {
  expect(generateRandomizedArray(1)).toEqual([0])
})

test('generate randomized array of 10 elements', () => {
  const arr = generateRandomizedArray(10)
  arr.sort((a, b) => a - b)
  expect(arr).toEqual([...Array(10).keys()])
})
