import React from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Navbar from 'react-bootstrap/Navbar'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
  faPlayCircle,
  faPauseCircle
} from '@fortawesome/free-regular-svg-icons'
import './NavBar.scss'
import {
  stringToSortType,
  PartialSortOptions,
  SortOptions
} from '../sorting/options'

type NavBarProps = {
  sortOptions: SortOptions
  onSortOptionsChange: (newSOProps: PartialSortOptions) => void
}

const NavBar = ({sortOptions, onSortOptionsChange}: NavBarProps) => {

  return (
    <Navbar bg="dark" variant="dark" expand="md">
      <Navbar.Brand href="/">Reactive Visual Sorter</Navbar.Brand>
      <Navbar.Toggle aria-controls="navbar-nav" />
      <Navbar.Collapse id="navbar-nav">
        <Form
          className="d-flex justify-content-around align-items-center ml-auto"
          inline
        >
          <Form.Group className="m-2" controlId="formSortSize">
            <Form.Label className="text-light pr-2">
              Sample size:
            </Form.Label>
            <input
              type="range"
              className="d-flex align-items-center custom-range"
              id="formSortSize"
              min={2}
              max={40}
              value={sortOptions.sampleSize}
              disabled={sortOptions.isRunning}
              onChange={(e) => {
                onSortOptionsChange({
                  sampleSize: parseInt(e.target.value)
                })
              }
              }
            />
          </Form.Group>
          <Form.Group className="m-2" controlId="formSortType">
            <Form.Label className="text-light pr-2">
              Sort type:
            </Form.Label>
            <Form.Control
              className="mr-4"
              as="select"
              value={sortOptions.sortType}
              disabled={sortOptions.isRunning}
              onChange={(ev) => {
                const newSortType = stringToSortType(ev.currentTarget.value)
                if (newSortType) {
                  onSortOptionsChange({
                    sortType: newSortType
                  })
                }
              }
              }
            >
              <option value="bubble">Bubble sort</option>
              <option value="insertion">Insertion sort</option>
              <option value="quick">Quicksort</option>
              <option value="selection">Selection sort</option>
            </Form.Control>
          </Form.Group>
          <Button
            id="run-button"
            variant="primary"
            active={sortOptions.isRunning}
            onClick={() => {
              onSortOptionsChange({
                isRunning: !sortOptions.isRunning
              })
            }
            }
          >
            Run
            <FontAwesomeIcon
              icon={sortOptions.isRunning ?
                faPauseCircle : faPlayCircle}
            />
          </Button>
        </Form>
      </Navbar.Collapse>
    </Navbar >
  )
}

export default NavBar
