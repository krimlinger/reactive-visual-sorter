import React, {useState, useRef, useEffect} from 'react'
import {useInterval} from 'react-use'
import * as d3 from 'd3'
import './BarGraph.scss'
import Bar from './Bar'
import {isSorted, generateRandomizedArray} from '../sorting/utils'
import {sortTypeToFunc, SortOptions} from '../sorting/options'

type BarGraphProps = {
  sortOptions: SortOptions
  onSortFinished: () => void
}

const
  height = 400,
  yPadding = 10,
  maxWidth = 800,
  minWidth = 300

function createXScale(sampleSize: number, width: number) {
  return d3.scaleBand<number>()
    .domain(d3.range(sampleSize))
    .range([0, width])
    .paddingInner(0.1)
}

function normalizedWidth(width: number) {
  if (width <= (minWidth - 2 * yPadding)) {
    return minWidth - 2 * yPadding;
  }
  if (width <= (maxWidth - 2 * yPadding)) {
    return width - 2 * yPadding
  }
  return maxWidth - 2 * yPadding
}

function createYScale(sampleSize: number, height: number) {
  return d3.scaleBand<number>()
    .domain(d3.range(sampleSize))
    .range([5, height - yPadding])
}

function sortSpeed(x: number) {
  return 0.298 * (x ** 2) - 24.368 * x + 747.54;
}

const BarGraph = ({
  sortOptions: {
    sampleSize,
    isRunning,
    sortType,
  },
  onSortFinished,
}: BarGraphProps) => {
  const [graph, setGraph] = useState(generateRandomizedArray(sampleSize))
  const sortFunc = sortTypeToFunc(sortType)
  const sortStepGen = useRef(sortFunc(graph)!)
  const [sortGen, setSortGen] = useState(sortFunc(graph)!.next())
  const [width, setWidth] = useState(
    normalizedWidth(document.documentElement.clientWidth))

  const xScale = createXScale(sampleSize, width)
  const yScale = createYScale(sampleSize, height)

  useEffect(() => {
    const updateWidth = () => {
      if (width <= (800 - 2 * yPadding)) {
        setWidth(normalizedWidth(document.documentElement.clientWidth))
      }
    }
    window.addEventListener('resize', updateWidth)
    return () => window.removeEventListener('resize', updateWidth)
  }, [width])

  useEffect(() => {
    setGraph(generateRandomizedArray(sampleSize))
  }, [sampleSize])

  useEffect(() => {
    if (!isRunning) {
      const sortFunc = sortTypeToFunc(sortType)
      sortStepGen.current = sortFunc(graph)!
      setSortGen(sortStepGen.current.next())
    }
  }, [graph, sortType, isRunning])

  useInterval(() => {
    if (!sortGen.done) {
      const newGraph = [...sortGen.value.array]
      setSortGen(sortStepGen.current.next())
      setGraph(newGraph)
    } else {
      onSortFinished()
    }
  }, isRunning ? sortSpeed(sampleSize) : null)

  const swappedIndices: number[] = []
  // Check if graph is sorted before showing swapped indices
  // because some sorting algorithms like quicksort will
  // show swapped indices even after the array being sorted.
  if (!isSorted(graph) &&
    sortGen.value &&
    sortGen.value.hasOwnProperty('swap')) {
    // @ts-ignore
    swappedIndices.push(...sortGen.value.swap)
  }

  return (
    <div
      id="graph"
      className="d-flex m-auto pt-5 justify-content-center"
    >
      <svg height={height}>
        {graph.map((v, i) => {
          return (
            <Bar
              key={v}
              value={v}
              order={i}
              height={yScale(v)!}
              width={xScale.bandwidth()}
              x={xScale(i)!}
              y={height - yScale(v)!}
              isSwapped={swappedIndices.includes(i)}
            />
          )
        })
        }
      </svg>
    </div>
  )
}

export default BarGraph
