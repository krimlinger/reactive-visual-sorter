import React from 'react'
import {useSpring, animated} from 'react-spring'
//import './Bar.scss'

type BarProps = {
  value: number
  order: number
  width: number
  height: number
  x: number
  y: number
  isSwapped: boolean
}

type BarColorScheme = {
  default: string
  swapped: string
}

const barColor: BarColorScheme = {
  default: '#43dde6',
  swapped: '#364f6b',
}
//#fc5185
//#f0f0f0

function numberToPixelWidth(n: number) {
  let count = 0;

  do {
    n = Math.floor(n/10)
    count++;
  } while(n > 0)

  return 5*count
}

const Bar = (props: BarProps) => {
  const updateAnimation = useSpring({
    opacity: 1,
    fill: (props.isSwapped ? barColor.swapped : barColor.default),
    from: {opacity: 0},
  })

  return (
    <>
      <animated.rect
        className="bar"
        fill="#0cad77"
        x={props.x}
        y={props.y}
        height={props.height}
        width={props.width}
        {...updateAnimation}
      />
      <text
        className="bar-label"
        x={props.x + (props.width / 2) - numberToPixelWidth(props.value)}
        y={props.y - 5}
      >
        {props.value}
      </text>
    </>
  )
}

export default Bar
