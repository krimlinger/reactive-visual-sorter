import React, {useState} from 'react'
import './App.scss'
import NavBar from './NavBar'
import BarGraph from './BarGraph'
import {
  PartialSortOptions,
  defaultSortOptions
} from '../sorting/options'

type AppProps = {}

const App = (_: AppProps) => {
  const [sortOptions, setSortOptions] = useState(defaultSortOptions)

  const handleSortOptionsChange = (newSOProps: PartialSortOptions) => {
    setSortOptions({...sortOptions, ...newSOProps})
  }

  const handleSortFinished = () => {
    handleSortOptionsChange({ isRunning: false})
  }

  return (
    <>
      <NavBar
        sortOptions={sortOptions}
        onSortOptionsChange={handleSortOptionsChange}
      />
      <BarGraph
        sortOptions={sortOptions}
        onSortFinished={handleSortFinished}
      />
    </>
  )
}

export default App
